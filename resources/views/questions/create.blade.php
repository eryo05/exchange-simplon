@extends('layouts.fullwidth')



@section('title', 'Poser une question')



@section('content')



    <div class="page-content ask-question">

        <div class="boxedtitle page-title"><h2>Ask Question</h2></div>



        <p>Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque.</p>



        <div class="form-style form-style-3" id="question-submit">

            {!! Form::open(['action' => 'QuestionController@store']) !!}

                <div class="form-inputs clearfix">

                    <p>

                        {!! Form::label('title', 'Titre de la Question', ['class' => 'required']) !!}

                        {{ Form::text('title', NULL, ['id' => 'question-title']) }}

                        <span class="form-description">Please choose an appropriate title for the question to answer it even easier .</span>

                    </p>

                    <p>

                         {!! Form::label('description', 'Description', ['class' => 'required']) !!}

                         {{ Form::text('description', NULL, ['id' => 'question_tags']) }}

                        <span class="form-description">Please choose suitable Keywords .</span>

                    </p>

                    <p>
                        {!! Html::decode(Form::label('category','Category: <span class="required">*</span>')) !!}

                        <span class="styled-select">

                                {!! Form::select('category',[
                                    'Back-end' => [
                                        'php' => 'PHP',
                                        'mysql' => 'MySQL',
                                        'nodejs' => 'NodeJS',
                                        'c#' => 'C#',
                                        'python' => 'Python',
                                        'ruby' => 'Ruby',
                                        'other' => 'Other'
                                    ],
                                    'Front-end' => [
                                        'html' => 'HTML',
                                        'css' => 'CSS',
                                        'sass' => 'SASS',
                                        'js' => 'JS',
                                        'other' => 'Other'
                                    ],
                                    'miscellaneous' => [
                                        'agile' => 'Agile',
                                        'sysadmin' => 'Admin',
                                        'devops' => 'Dev Ops',
                                        'other' => 'Other'
                                    ],
                                ], null, ['placeholder' => 'Pick a category...']) !!}

                        </span>

                        <span class="form-description">Please choose the appropriate section so easily search for your question .</span>

                    </p>

                </div>

                <div id="form-textarea">

                    <p>

                        <label class="required">Details<span>*</span></label>

                        <textarea id="question-details" aria-required="true" cols="58" rows="8"></textarea>

                        <span class="form-description">Type the description thoroughly and in detail .</span>

                    </p>

                </div>

                <p class="form-submit">

                    <input type="submit" id="publish-question" value="Publish Your Question" class="button color small submit">

                </p>

            {!! Form::close() !!}

        </div>

    </div><!-- End page-content -->



@endsection