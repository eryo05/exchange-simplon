<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Question;


class QuestionRepository extends Repository
{
    public function __CONSTRUCT(Question $model){
        parent::__CONSTRUCT($model);
    }
    public function getOrderedQuestions(){
        return $this->model->orderBy('updated_at', 'desc')->get();
    }
    public function getRecentQuestions($nbr)
    {
        return $this->model->orderBy('updated_at', 'desc')->take($nbr)->get();
    }

    public function countAllQuestions()
    {
        return $this->model->count();
    }
}
?>