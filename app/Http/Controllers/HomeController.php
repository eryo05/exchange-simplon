<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\QuestionRepository;
use App\Repositories\AnswerRepository;

class HomeController extends Controller
{
    protected $questionRepository;

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = $this->questionRepository->getOrderedQuestions();
        $recentQuestions = $this->questionRepository->getRecentQuestions(2);
        $countAllQuestions = $this->questionRepository->countAllQuestions();
        return view('homepage', compact('questions', 'recentQuestions','countAllQuestions'));
    }
    
}
