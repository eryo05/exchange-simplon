<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpvote;
use App\Http\Controllers\Controller;
use App\Repositories\UpvoteRepository;

class UpvoteController extends Controller
{
    public function __construct(UpvoteRepository $upvoteRepository)
    {
        $this->upvoteRepository = $upvoteRepository;
    }

    public function store($idAnswer, StoreUpvote $request)
    {
        
        $requestData = $request->all();

        $requestData["answer_id"] = $idAnswer;
        
        $this->upvoteRepository->create($requestData);

        return back();
    }
}
