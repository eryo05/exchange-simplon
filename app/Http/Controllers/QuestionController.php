<?php namespace App\Http\Controllers;

use App\Repositories\QuestionRepository;
use App\Repositories\AnswerRepository;
use App\Http\Requests\StoreQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\AnswerController;

class QuestionController extends Controller
{
    protected $questionRepository;

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    public function index()
    {
        $questions = $this->questionRepository->getOrderedQuestions();
        $recentQuestions  = $this->questionRepository->getRecentQuestions(2);
        return view('questions.index', compact('questions', 'recentQuestions'));
    }

    public function create()
    {
        return view('questions.create');
    }

    public function show($id, AnswerRepository $answerRepository)
    {
        $question = $this->questionRepository->show($id);
        $answers = $answerRepository->getOrdered($id);
        $nbrAnswers = $answerRepository->count($id);
        $countAllQuestions = $this->questionRepository->countAllQuestions();
        return view('questions.show', compact('question', 'answers', 'nbrAnswers', 'countAllQuestions'));
        
    }

    public function store(StoreQuestion $request)
    {
        $requestData = $request->all();
        $question = $this->questionRepository->create($requestData);

        return redirect()->route('questions.show', ['id' => $question->id])->with('flash_message', 'Question added!');
    }

}

