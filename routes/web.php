<?php

use App\Http\Controllers\QuestionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
Route::get('/poser-une-question', 'QuestionController@create')->name('poser-une-question');
Route::get('/questions', 'QuestionController@index');

Route::post('/QuestionController', 'QuestionController@store');
Route::resource('questions', 'QuestionController')->only(['show', 'index']);
Route::post('/HomeController', 'AnswerController@store');

Route::post('answer/upvote/{id}', 'UpvoteController@store');